const express = require('express');
const app = express();
const port = 8080;
const bodyparser = require('body-parser')
app.use(bodyparser.urlencoded({extended:false}))
app.use(bodyparser.json())
require('dotenv').config()
require("./routes/data").getRouter(app)
app.listen(port,()=>{
    console.log(`Port running on ${port}!`)
})