var express = require('express')
var contr = require('../controller/data');


exports.getRouter =(app) => {

    app.route("/encryptData").post(contr.encryptData)
    app.route("/decryptData").post(contr.decryptData)
    
}


