const crypto = require('crypto');
exports.encryptData = async function(req,res){
    try{
        let cipher = crypto.createCipheriv('aes-256-cbc', process.env.ENC_KEY, process.env.IV);
        let encrypted = cipher.update(req.body.text, 'utf8', 'base64');
        encrypted += cipher.final('base64');
            res.status(200).json({ data : encrypted })
    }catch(error){
        res.status(403).send(error)
        console.log(error)
       
}
     }


exports.decryptData = async function(req,res){
    try{
        let decipher = crypto.createDecipheriv('aes-256-cbc', process.env.ENC_KEY, process.env.IV);
        let decrypted = decipher.update(req.body.hash, 'base64', 'utf8');
        res.status(200).json({ data : decrypted + decipher.final('utf8') })
    }catch(error){
        res.status(403).send(error)
    }
}

